FROM python:3.11

RUN \
  apt-get update && \
  apt-get upgrade --yes && \

COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt && rm /tmp/requirements.txt

COPY . /api
WORKDIR /api

CMD python -u local.py
